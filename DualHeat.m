% DualHeat: Constructs an object for DualHeat device
% DualHeat class contains the following public methods:
%       
%        .setFanSpeedPerc (see <a href="matlab:help DualHeat.setFanSpeedPerc">DualHeat.setFanSpeedPerc</a>)
%        .setInternalSamplingFreq (see <a href="matlab:help DualHeat.setInternalSamplingFreq">DualHeat.setInternalSamplingFreq</a>)
%        .setArtificialNoise (see <a href="matlab:help DualHeat.setArtificialNoise">DualHeat.setArtificialNoise</a>)
%        .setDelaySamples (see <a href="matlab:help DualHeat.setDelaySamples">DualHeat.setDelaySamples</a>)
%        .setFilter (see <a href="matlab:help DualHeat.setFilter">DualHeat.setFilter</a>)
%        .dataFlowOnOff (see <a href="matlab:help DualHeat.dataFlowOnOff">DualHeat.dataFlowOnOff</a>)
%        .setVerboseModeOnOff (see <a href="matlab:help DualHeat.setVerboseModeOnOff">DualHeat.setVerboseModeOnOff</a>)
%        .calibrate (see <a href="matlab:help DualHeat.calibrate">DualHeat.calibrate</a>)
%        .getBendPerc (see <a href="matlab:help DualHeat.getBendPerc">DualHeat.getBendPerc</a>)
%        .getUserInputLPerc (see <a href="matlab:help DualHeat.getUserInputLPerc">DualHeat.getUserInputLPerc</a>)
%        .getUserInputRPerc (see <a href="matlab:help DualHeat.getUserInputRPerc">DualHeat.getUserInputRPerc</a>)
%        .getTerminalInputPerc (see <a href="matlab:help DualHeat.getTerminalInputPerc">DualHeat.getTerminalInputPerc</a>)
%        .off (see <a href="matlab:help DualHeat.off">DualHeat.off</a>)
%
% Description of each method can be displayed using
%
%        help DualHeat.[method name]
%
% Brought to you by Optimal Control Labs Ltd.


classdef DualHeat < handle
  
   properties(SetAccess=public)
        SerialObj                   % stores active serial object
        LastIncommingMessage        % last message from experiment
        VerboseMode
        DataFlow
        ProcessTemp
        AmbientTemp
%         BendPerc
%         UserInputLPerc
%         UserInputRPerc
%         TerminalInputPerc
        Version
   end
    
   properties(SetAccess=public)
        ComPort                     % COM port
        SerialStatus                % status of serial connection
   end
   
   properties(Constant)
        COM_BAUD_RATE = 115200      % fixed baud rate for usb-serial
   end
   
   methods(Access=public)
        
       function obj = DualHeat(varargin)
            % DualHeat constructor method
            if(length(varargin)==1)
               port = varargin{1};
               obj.ComPort = port;
            elseif(isempty(varargin))
               obj.findComPort();
            else
               error('Too many input arguments.'); 
            end
            obj.SerialStatus = 'Closed'; 
            obj.VerboseMode = 0;
            obj.initSerialObject(obj.ComPort);
            obj.serialConnect();
            obj.dataFlowOnOff(1);
            obj.DataFlow = 1;
       end
       
       function setRightInput(obj, value)
           satur = 200;
            if(value>satur)
                value = satur;
            elseif(value<0)
                value = 0;
            end
            heat = round(value);
            fprintf(obj.SerialObj,'<R:%d>', heat);
       end
       
       function setLeftInput(obj, value)
           % DualHeat.setFanSpeedPerc(SPEED) sets the speed of the fan on
           % a DualHeat device. SPEED is in percentage [0-100] of maximum
           % achievable fan speed.
           satur = 200;
            if(value>satur)
                value = satur;
            elseif(value<0)
                value = 0;
            end
            heat = round(value);
            fprintf(obj.SerialObj,'<L:%d>', heat);
       end
       
       function setInternalSamplingFreq(obj,freq)
            % DualHeat.setInternalSamplingFreq(FREQ) sets the frequency [in Hz] of
            % data update rate from DualHeat [min: 5, max: 255]
            if(freq>255)
                freq = 255;
            elseif(freq<5)
                freq = 5;
            end
            
            fprintf(obj.SerialObj,'<S:%d>',round(freq));
       end
       
       function dataFlowOnOff(obj,opt)
            % DualHeat.dataFlowOnOff(OPT) for binary value OPT (0-off,
            % 1-on) turns off/on the background serial
            % communication between DualHeat and MATLAB
            if(opt==1)
                fprintf(obj.SerialObj,'<P:%d>',round(opt));
                obj.DataFlow = 1;
            elseif(opt==0)
                fprintf(obj.SerialObj,'<P:%d>',round(opt));
                obj.DataFlow = 0;
            else
               warning('FlexyWarning:UnexpectedValueWarning','dataFlowOnOff function expects logical value (either 0 or 1)'); 
            end
       end
       
       function setVerboseModeOnOff(obj,opt)
            % DualHeat.setVerboseModeOnOff(OPT) for binary value OPT (0-off,
            % 1-on) turns off/on the console output of background serial
            % communication between DualHeat and MATLAB
          	if(opt==1)
                obj.VerboseMode = 1;
            elseif(opt==0)
                obj.VerboseMode = 0;
            else
               warning('FlexyWarning:UnexpectedValueWarning','setVerboseModeOnOff function expects logical value (either 0 or 1)'); 
            end  
       end
       
       function out = getProcessTemp(obj)
            out = obj.ProcessTemp;
       end
       
       function out = getAmbientTemp(obj)
            out = obj.AmbientTemp;
       end
       
       function off(obj)
            obj.setLeftInput(0);
            obj.setRightInput(0);
       end
       
       function connect(obj)
            obj.serialConnect();
       end
       
       function close(obj)
            % Closes active connection
            obj.serialClose();
       end
       
       function setPort(obj, port)
            obj.ComPort = port;
       end
       
   end
   
   methods(Static)
       
       
   
   end
   
   methods(Access=private)
       
       function findComPort(obj)
           vdate = version('-date');
           release_year = str2double(vdate(end-3:end));
           if(ispc&&release_year>=2017)
               ports = seriallist;
               disp('Searching COM ports ...')
               for i=1:length(ports)
                  ser = serial(ports{i});
                  set(ser,'BaudRate', obj.COM_BAUD_RATE);
                  set(ser,'Terminator',{'CR/LF',''});
                  set(ser, 'TimeOut', 0.1);
                  warning('off','all');
                  fopen(ser);
                  pause(4);
                  fprintf(ser,'<V:1>');
                  pause(0.1);
                  message = fscanf(ser,'%s');
                  warning('on','all')
                  [~,ver] = strtok(message,':');
                  obj.Version = ver(2:end-1);
                  obj.ComPort = ports{i};
                  if(~isempty(obj.Version))
                      disp([obj.Version ' found on port ' ports{i}]);
                      fclose(ser);
                      delete(ser);
                      break;
                  end
                  fclose(ser);
                  delete(ser);
               end
               if(isempty(obj.Version))
                  error('No DualHeat device found.');
               end
           else
               error('Specification of com. port is required. See <a href="matlab:help DualHeat">help DualHeat</a> for more information.')
           end
       end
           
       function getIncommingMessage(obj)
            % Parsing function for incomming messages
            message = fscanf(obj.SerialObj,'%s');
            [~,data] = strtok(message,':');
%             [d1,d234] = strtok(data,',');
%             [d2,d34] = strtok(d234,',');
            [d3,d4] = strtok(data,',');
            obj.ProcessTemp = str2double(d3(2:end));
%             obj.UserInputLPerc = str2double(d2(1:end));
%             obj.UserInputRPerc = str2double(d3(1:end));
            obj.AmbientTemp = str2double(d4(2:end-1));
            if(obj.VerboseMode)
                disp(message);
            end
            obj.LastIncommingMessage = message;
       end
       
       function initSerialObject(obj, com_port)
            % Initiates serial object
            obj.ComPort = com_port;
            obj.SerialObj = serial(com_port);
            set(obj.SerialObj,'BaudRate', obj.COM_BAUD_RATE);
            set(obj.SerialObj,'Terminator',{'CR/LF',''});
            obj.SerialObj.BytesAvailableFcnMode = 'terminator';
            obj.SerialObj.BytesAvailableFcn = @(~,evt)obj.getIncommingMessage();
       end
        
       function serialConnect(obj)
            % Establishes connection via serial interface
            fopen(obj.SerialObj); % this will reset the MCU, so we need to wait a while
            disp('Waiting for connection ...');
            pause(2);
            if(strcmp(obj.SerialObj.status,'open'))
                obj.SerialStatus = 'Open';
                disp('Connection established.')   
            else
                error('FlexyError:SerialConnectError','Could not establish the serial connection.\n %s port may be missing or used by other program.',obj.ComPort);    
            end
       end
       
       function serialClose(obj)
            % Closes serial connection
            fclose(obj.SerialObj);
            obj.SerialStatus = 'Closed';
            disp('Connection closed.');
       end
        
    end
    
end