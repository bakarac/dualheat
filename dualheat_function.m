function dualheat_function(block)

setup(block);

%endfunction

function setup(block)
% Register number of ports
block.NumInputPorts  = 2;
block.NumOutputPorts = 2;

% Setup port properties to be inherited or dynamic
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

% Register parameters
block.NumDialogPrms     = 2;
block.DialogPrmsTunable = {'Nontunable', ...
                           'Nontunable' ...
                          };

block.SampleTimes = [block.DialogPrm(1).Data 0];

block.SimStateCompliance = 'DefaultSimState';

block.RegBlockMethod('SetInputPortSamplingMode',@SetInputPortSamplingMode);
block.RegBlockMethod('PostPropagationSetup',    @DoPostPropSetup);
block.RegBlockMethod('InitializeConditions', @InitializeConditions);
block.RegBlockMethod('Start', @Start);
block.RegBlockMethod('Outputs', @Outputs);     % Required
block.RegBlockMethod('Update', @Update);
block.RegBlockMethod('Derivatives', @Derivatives);
block.RegBlockMethod('Terminate', @Terminate); % Required

%end setup

function SetInputPortSamplingMode(block, idx, fd)
  block.InputPort(idx).SamplingMode = fd;
  block.OutputPort(1).SamplingMode = fd;
  block.OutputPort(2).SamplingMode = fd;

function DoPostPropSetup(block)
    if block.SampleTimes(1) == 0
        throw(MSLException(block.BlockHandle,'Dicrete sampling time required'));
    end

function InitializeConditions(block)
    block.OutputPort(1).Data = 0;
    block.OutputPort(2).Data = 0;

%end InitializeConditions

function Start(block)
global dualheat_instance;

    dualheat_instance = DualHeat(block.DialogPrm(2).Data);
    pause(4);
    dualheat_instance.setVerboseModeOnOff(0); %debugging
    dualheat_instance.setInternalSamplingFreq(round(1/block.DialogPrm(1).Data));
%endfunction

function Outputs(block)
global dualheat_instance;

dualheat_instance.setRightInput(block.InputPort(1).Data);
dualheat_instance.setLeftInput(block.InputPort(2).Data);
block.OutputPort(1).Data = dualheat_instance.getProcessTemp;
block.OutputPort(2).Data = dualheat_instance.getAmbientTemp;
%end Outputs

function Update(block)

%end Update

function Derivatives(block)

%end Derivatives

function Terminate(block)
    global dualheat_instance;
    dualheat_instance.off();
    dualheat_instance.close();
    dualheat_instance.delete();
    
 %end Terminate

